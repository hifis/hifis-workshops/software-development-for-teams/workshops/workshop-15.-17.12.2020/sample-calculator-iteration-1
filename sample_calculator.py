from decimal import Decimal
import sys


""" Simple prototype of the SampleCalculator. """


def _read_sample_values():
    """ Reads the sample values from console or file. """

    if len(sys.argv) == 1:  # read values from commandline
        sample_values = input("Please, enter the sample values:\n")
        sample_values = [Decimal(value.strip()) for value in sample_values.split(";")]
    else:  # read values from file
        sample_values = list()
        with open(sys.argv[1], "r", encoding="utf-8") as file_object:
            lines = file_object.readlines()
            for line in lines:
                values = line.split(";")
                for value in values:
                    value = value.strip()
                    if len(value) > 0:
                        sample_values.append(Decimal(value))
    return sample_values


def _calculate_average(sample_values):
    """ Calculates the average. """

    sum = Decimal(0)
    for sample_value in sample_values:
        sum += sample_value
    return sum / len(sample_values)


def _calculate_variance(sample_values, average):
    """ Calculates the variance. """

    variance = Decimal(0)
    for sample_value in sample_values:
        variance += (sample_value - average) ** 2
    return variance / (len(sample_values) - 1)


def _calculate_deviation(variance):
    """ Calculates the standard degression. """

    return variance ** Decimal("0.5")


def main():
    """ Main entry point of the function. """

    # Read sample values
    sample_values = _read_sample_values()

    # Calculate results
    average = _calculate_average(sample_values)
    variance = _calculate_variance(sample_values, average)
    standard_degression = _calculate_deviation(variance)

    # Print results on the console
    print("average:", average)
    print("variance:", variance)
    print("deviation:", standard_degression)


if __name__ == "__main__":
    main()
